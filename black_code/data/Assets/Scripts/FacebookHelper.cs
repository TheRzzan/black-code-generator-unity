﻿using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using UnityEngine;

public class FacebookHelper : MonoBehaviour
{
    public delegate void EmmitDataFetched();

    private EmmitDataFetched mCallback;

    public Dictionary<string, string> resultHashMap = new Dictionary<string, string>();

    public void FetchAppLinkData(EmmitDataFetched callback, string fbAppId = null)
    {
        mCallback = callback;
        InitFB(fbAppId);
    }
     
    private void InitFB(string fbAppId)
    {
        if (!FB.IsInitialized)
        {
            if (fbAppId != null)
            {
                FB.Init(fbAppId, null, true, true, true, false, true, null, "en_US", OnHideUnity, InitCallback);
            }
            else
            {
                FB.Init(InitCallback, OnHideUnity);
            }
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            FBInited();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    private void FBInited()
    {
        Debug.Log("FB inited");
        FB.Mobile.FetchDeferredAppLinkData(DeepLinkCallback);
    }

    void DeepLinkCallback(IAppLinkResult result)
    {
        /*if (!string.IsNullOrEmpty(result.TargetUrl))
        {
            resultHashMap.Add("deep_fb", result.TargetUrl);
            Uri myUri = new Uri(result.TargetUrl);
            NameValueCollection query = HttpUtility.ParseQueryString(myUri.Query);
            foreach (string key in query.AllKeys)
            {
                resultHashMap.Add(key, query.Get(key));
            }
        }*/
        Debug.Log("DeepLinkCallback");
        resultHashMap.Add("deep_fb", result.TargetUrl);
        mCallback?.Invoke();
    }
}
 