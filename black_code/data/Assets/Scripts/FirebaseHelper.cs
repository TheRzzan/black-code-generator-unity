﻿using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
 
public class FirebaseHelper : MonoBehaviour
{
    public delegate void EmmitDataFetched();

    private FirebaseApp firebaseApp;
    public string mUrl = null;
    public string appsflyerApiKey = null;
    public string fbAppId = null;

    public const string EMPTY = "empty.f.value";
    public const string F_ROOT = "F_CONFIG_ROOT";
    public const string F_URL_1 = "F_CONFIG_U1";
    public const string F_URL_2 = "F_CONFIG_U2";
    public const string F_APPS = "appsflyer_api_key";
    public const string FB_APP_ID = "facebook_app_id";

    public void InitFirebase(EmmitDataFetched callback)
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                firebaseApp = FirebaseApp.DefaultInstance;
                InitCloudMessaging();
                FetchDatabase(callback);
            }
            else
            {
                callback();
                Debug.Log(string.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }
        });
    }
    
    private void InitCloudMessaging()
    {
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
    }

    private void FetchDatabase(EmmitDataFetched callback)
    {
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase.DefaultInstance
            .GetReference(F_ROOT)
            .GetValueAsync().ContinueWithOnMainThread(task => {
                if (task.IsFaulted)
                    Debug.Log("Database error");
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    mUrl = (snapshot.HasChild(F_URL_1) ? snapshot.Child(F_URL_1).Value.ToString() : "") + 
                    (snapshot.HasChild(F_URL_2) ? snapshot.Child(F_URL_2).Value.ToString() : "");
                    if (mUrl.Length <= 0)
                        mUrl = EMPTY;

                    appsflyerApiKey = snapshot.HasChild(F_APPS) ? snapshot.Child(F_APPS).Value.ToString() : EMPTY;
                    fbAppId = snapshot.HasChild(FB_APP_ID) ? snapshot.Child(FB_APP_ID).Value.ToString() : EMPTY;
                }
                callback();
            });
    }
}
