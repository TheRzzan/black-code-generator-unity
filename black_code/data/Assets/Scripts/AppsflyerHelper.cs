﻿using AppsFlyerSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsflyerHelper : MonoBehaviour, IAppsFlyerConversionData
{
    public delegate void EmmitDataFetched();

    private EmmitDataFetched mCallback;

    public Dictionary<string, string> resultHashMap = new Dictionary<string, string>();

    private const string MEDIA_SOURCE = "media_source";

    public void FetchConversionData(EmmitDataFetched callback, string devKey)
    {
        mCallback = callback;
        AppsFlyer.initSDK(devKey, "BUNDLE_PARAM_ID", this);
        AppsFlyer.startSDK();
    }
   
    public void onConversionDataSuccess(string conversionData)
    {
        AppsFlyer.AFLog("onConversionDataSuccess", conversionData);
        Dictionary<string, object> conversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
        if (conversionDataDictionary.ContainsKey(MEDIA_SOURCE))
        {
            foreach (string key in conversionDataDictionary.Keys)
            {
                if (conversionDataDictionary[key] is string)
                    resultHashMap.Add(key, conversionDataDictionary[key] as string);
            }
        }
        mCallback();
    }

    public void onConversionDataFail(string error)
    {
        AppsFlyer.AFLog("onConversionDataFail", error);
        mCallback();
    }

    public void onAppOpenAttribution(string attributionData)
    {
        AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
        Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
    }
    public void onAppOpenAttributionFailure(string error)
    {
        AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
    }
}
