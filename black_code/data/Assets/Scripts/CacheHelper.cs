﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CacheHelper : MonoBehaviour
{
    public delegate void EmmitDataFetched();

    private string mUrl1;
    private string mUrl2;
    private bool firstIn;
    private bool mOpenWV;

    private const string U1_KEY = "u1.aaa.key";
    private const string U2_KEY = "u2.bbb.key";
    private const string U_STACK_KEY = "u.stack.ccc.key";
    private const string U_STACK_SEPA_KEY = "u.stack.sepa.ccc.key";
    public static string STACK_SAVE = "saveStack=true";
    private const string OWV_KEY = "owv.bbb.key";
    private const string FIN_KEY = "fin.ccc.key";

    private void Awake()
    {
        Debug.Log("Set JS callback");
#if !UNITY_EDITOR && UNITY_ANDROID
        Debug.Log("Set JS callback android");
        (new AndroidJavaObject("net.gree.unitywebview.Utils")).CallStatic("setPageStartedCallback", new AndroidJavaRunnable(PageStarted));
#endif
    }
     
    private void PageStarted()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
      if (mUrl1.Contains(STACK_SAVE))
        {
            string mUrl2TMP = (new AndroidJavaObject("net.gree.unitywebview.Utils")).GetStatic<string>("lastPageUrl");
            if (mUrl2TMP != mUrl2)
            {
                string stackBefore = PlayerPrefs.GetString(U_STACK_KEY, "");
                if (stackBefore.Length > 0)
                    PlayerPrefs.SetString(U_STACK_KEY, stackBefore + U_STACK_SEPA_KEY + mUrl2);
                else
                    PlayerPrefs.SetString(U_STACK_KEY, mUrl2);
                mUrl2 = mUrl2TMP;
                PlayerPrefs.SetString(U2_KEY, mUrl2);
            }
        }
#endif
    }

    public void PopUrlStack()
    {
        string stackTmp = PlayerPrefs.GetString(U_STACK_KEY);
        if (stackTmp.Length <= 0)
        {
            return;
        }
        if (!stackTmp.Contains(U_STACK_SEPA_KEY))
        {
            mUrl2 = stackTmp;
            PlayerPrefs.SetString(U2_KEY, mUrl2);
            return;
        }
        string[] splitList = stackTmp.Split(new string[] { U_STACK_SEPA_KEY }, StringSplitOptions.None);
        mUrl2 = splitList[splitList.Length - 1];
        PlayerPrefs.SetString(U2_KEY, mUrl2);
        string newVal = splitList[0];
        for (int i = 1; i < splitList.Length - 1; i++)
        {
            newVal += U_STACK_SEPA_KEY + splitList[i];
        }
        PlayerPrefs.SetString(U_STACK_KEY, newVal);
    }
    
    public string GetU1()
    {
        return mUrl1;
    }

    public string GetU2()
    {
        return mUrl2;
    }

    public bool GetFIN()
    {
        return firstIn;
    }

    public bool GetOWV()
    {
        return mOpenWV;
    }

    public void SaveData(string url1, string url2, bool openWV)
    {
        mUrl1 = url1;
        mUrl2 = url2;
        mOpenWV = openWV;
        if (mUrl1 != null && mUrl1.Length <= 0)
            mUrl1 = null;
        if (mUrl2 != null && mUrl2.Length <= 0)
            mUrl2 = null;
        PlayerPrefs.SetString(U1_KEY, mUrl1);
        PlayerPrefs.SetString(U2_KEY, mUrl2);
        PlayerPrefs.SetInt(OWV_KEY, openWV ? 1 : 0);
    }

    public void FetchData(EmmitDataFetched callback)
    {
        mUrl1 = PlayerPrefs.GetString(U1_KEY);
        mUrl2 = PlayerPrefs.GetString(U2_KEY);
        if (mUrl1 != null && mUrl1.Length <= 0)
            mUrl1 = null;
        if (mUrl2 != null && mUrl2.Length <= 0)
            mUrl2 = null;
        firstIn = PlayerPrefs.GetInt(FIN_KEY, 1) == 1;
        mOpenWV = PlayerPrefs.GetInt(OWV_KEY, 1) == 1;
        PlayerPrefs.SetInt(FIN_KEY, 0);
        callback();
    }
}
