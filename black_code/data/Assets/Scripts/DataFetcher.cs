using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DataFetcher : MonoBehaviour
{
    public GameObject webView;
    public Image[] colorRand;

    public FirebaseHelper firebaseHelper;
    public AppsflyerHelper appsflyerHelper;
    public BackendHelper backendHelper;
    public CacheHelper cacheHelper;
    public FacebookHelper facebookHelper;

    void Start()
    {
        webView.GetComponent<MyWebView>().InitWV();
        foreach (Image img in colorRand)
            img.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        cacheHelper.FetchData(CacheDataFetched);
    }

    private void CacheDataFetched()
    {
        if (cacheHelper.GetFIN())
            firebaseHelper.InitFirebase(FirebaseDataFetched);
        else
            CheckDataAndOpen();
    }

    private void FirebaseDataFetched()
    {
        InitAppsflyer();
    }

    private void InitAppsflyer()
    {
        if (firebaseHelper.appsflyerApiKey == FirebaseHelper.EMPTY)
            InitFacebook();
        else
            appsflyerHelper.FetchConversionData(AppsflyerDataFetched, firebaseHelper.appsflyerApiKey);
    }

    private void AppsflyerDataFetched()
    {
        InitFacebook();
    }

    private void InitFacebook()
    {
        if (firebaseHelper.fbAppId == FirebaseHelper.EMPTY)
            facebookHelper.FetchAppLinkData(FacebookDataFetched);
        else
            facebookHelper.FetchAppLinkData(FacebookDataFetched, firebaseHelper.fbAppId);
    }

    private void FacebookDataFetched()
    {
        if (appsflyerHelper.resultHashMap.Count > 0) {
            if (facebookHelper.resultHashMap.Count > 0)
                appsflyerHelper.resultHashMap.Add("deep_fb", facebookHelper.resultHashMap["deep_fb"]);
            backendHelper.GetBackendUrls(BackEndDataFetched, firebaseHelper.mUrl, appsflyerHelper.resultHashMap);
        } else
            backendHelper.GetBackendUrls(BackEndDataFetched, firebaseHelper.mUrl, facebookHelper.resultHashMap);
    }

    private void BackEndDataFetched(string u1, string u2, bool openWV)
    {
        cacheHelper.SaveData(u1, u2, openWV);
        CheckDataAndOpen();
    }

    private void CheckDataAndOpen()
    {
        if (cacheHelper.GetU1() != null)
            OpenWebView();
        else
            OpenGame();
    }

    private void OpenWebView()
    {
        if (cacheHelper.GetFIN() || cacheHelper.GetU2() == null)
            webView.GetComponent<MyWebView>().Url = cacheHelper.GetU1();
        else
            webView.GetComponent<MyWebView>().Url = cacheHelper.GetU2();
        webView.SetActive(true);
    }

    private void OpenGame()
    {
        SceneManager.LoadScene("CLOACA_NAME");
    }
}
