import os
import shutil
from black_code import file_editor, gitlab_helper, xml_helper

def add_black_code(dst, chat_id, message):
    app_number = xml_helper.get_tag_first_val(message, xml_helper.appNumber)
    copytree('/code/black_code/data', dst)
    file_editor.edit_in_root(dst, chat_id, message)
    gitlab_helper.push_build(dst, app_number)

def copytree(src, dst, symlinks=False, ignore=None):
    print(src)
    for root, subdirs, files in os.walk(src):
        for subdir in subdirs:
            try:
                s = os.path.join(root, subdir)
                d = os.path.join(root.replace(src, dst), subdir)
                shutil.copytree(s, d, symlinks, ignore)
            except FileExistsError as e:
                print(e)
        for file in files:
            try:
                s = os.path.join(root, file)
                d = os.path.join(root.replace(src, dst), file)
                shutil.copy2(s, d)
            except FileExistsError as e:
                print(e)
