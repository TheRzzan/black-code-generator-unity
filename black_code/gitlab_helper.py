import os
import subprocess

def push_build(root, app_number):
    os.chdir(root)
    subprocess.call("git init", shell=True)
    subprocess.call("git remote add origin git@gitlab.com:TheRzzan/unity-builder.git", shell=True)
    subprocess.call("git pull origin master", shell=True)
    subprocess.call("git branch " + app_number, shell=True)
    subprocess.call("git checkout " + app_number, shell=True)
    subprocess.call("git add .", shell=True)
    subprocess.call("git commit -m \"Black code added.\"", shell=True)
    subprocess.call("git push -u origin --force " + app_number, shell=True)
