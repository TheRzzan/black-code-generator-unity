import ast
import re

from black_code import xml_helper

build_before = "  m_Scenes:\n"
build_after = "  m_Scenes:\n" \
              "  - enabled: 1\n" \
              "    path: Assets/Scenes/Splash.unity\n" \
              "    guid: 1c5678f0f3f9bf245a7cf0b2f5934565\n"

def edit_in_root(root, chat_id, message):
    edit_proj_settings(root)
    edit_build_settings(root)
    edit_gitlab_ci(root, chat_id, message)
    edit_cloaca_name(root, message)
    edit_google_services_json(root, message)
    edit_bundle_id(root, message)
    edit_firebase_config(root, message)
    edit_backend_config(root, message)
    edit_manifest(root)
    edit_fb_app_id(root, message)
    edit_splash_icon_meta(root)

def edit_proj_settings(root):
    try:
        before1 = 'androidRenderOutsideSafeArea: ' + '+[^\\n]+'
        after1 = 'androidRenderOutsideSafeArea: 0'

        before2 = 'defaultScreenOrientation: ' + '+[^\\n]+'
        after2 = 'defaultScreenOrientation: 4'

        before3 = 'scriptingBackend:\n' \
                  '    Android: ' + '+[^\\n]+'
        after3 = 'scriptingBackend:\n' \
                 '    Android: 1'

        before4 = 'AndroidTargetArchitectures: ' + '+[^\\n]+'
        after4 = 'AndroidTargetArchitectures: 3'

        before5 = 'apiCompatibilityLevel: ' + '+[^\\n]+'
        after5 = 'apiCompatibilityLevel: 6'

        before7 = 'apiCompatibilityLevelPerPlatform:\n' \
                  '    Android: ' + '+[^\\n]+'
        after7 = 'apiCompatibilityLevelPerPlatform:\n' \
                 '    Android: 3'

        before6 = 'AndroidMinSdkVersion: ' + '+[^\\n]+'
        after6 = 'AndroidMinSdkVersion: 21'

        before8 = 'm_Icon: ' + '+[^\\n]+'
        after8 = 'm_Icon: {fileID: 2800000, guid: 1ae6bee80c27b9d499cc268f3a8f46fe, type: 3}'

        before9 = 'scriptingBackend: {}'

        path = root + "ProjectSettings/ProjectSettings.asset"
        with open(path, 'r') as file:
            data = file.read()
        try:
            data = data.replace(re.findall(before1, data)[0], after1)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before2, data)[0], after2)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before3, data)[0], after3)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before4, data)[0], after4)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before5, data)[0], after5)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before6, data)[0], after6)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before7, data)[0], after7)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before8, data)[0], after8)
        except Exception as e:
            print(e)
        try:
            data = data.replace(re.findall(before9, data)[0], after3)
        except Exception as e:
            print(e)
        with open(path, 'w') as file:
            file.write(data)
    except Exception as exc:
        print(exc)

def edit_splash_icon_meta(root):
    try:
        with open('/code/black_code/data/Assets/Sprites/Icon.png.meta', 'r') as file:
            data = file.read()
        with open(root + "Assets/Sprites/Icon.png.meta", 'w') as file:
            file.write(data)
    except Exception as e:
        print(e)

def edit_fb_app_id(root, message):
    before = 'FB_SET_APP_ID'
    before2 = '420191489307070'
    after = xml_helper.get_tag_first_val(message, xml_helper.facebookAppId)
    p = root + "Assets/FacebookSDK/SDK/Resources/FacebookSettings.asset"
    p2 = root + "Assets/Plugins/Android/AndroidManifest.xml"
    replace_str(p, before, after)
    replace_str(p2, before2, after)

def edit_manifest(root):
    new_line = '    "jillejr.newtonsoft.json-for-unity": "https://github.com/jilleJr/Newtonsoft.Json-for-Unity.git#upm",\n'
    dst = root + "Packages/manifest.json"
    try:
        with open(dst, 'r') as file:
            data = file.readlines()

        if data[2] != new_line:
            data[2] = new_line + data[2]

        with open(dst, 'w') as file:
            file.writelines(data)
    except Exception as e:
        print(e)

def edit_gitlab_ci(root, chat_id, message):
    try:
        app_number = xml_helper.get_tag_first_val(message, xml_helper.appNumber)
        key_pass = xml_helper.get_tag_first_val(message, xml_helper.keystorePass)
        name = xml_helper.get_tag_first_val(message, xml_helper.maintainerName)
        surname = xml_helper.get_tag_first_val(message, xml_helper.maintainerSurname)
        package_name = xml_helper.get_tag_first_val(message, xml_helper.appBundle)
        if app_number is not None:
            p = root + ".gitlab-ci.yml"
            replace_str(p, "APP_NUMBER", app_number)
            replace_str(p, "YOUR_ID", str(chat_id))
            replace_str(p, "YOUR_TOKEN", '1689931385:AAFoHYKvtm4I6nAeEQNhueity6Z49taNBms')
            replace_str(p, "_KEY_PASS_", key_pass)
            replace_str(p, "MAIN_NAME", name)
            replace_str(p, "MAIN_SURNAME", surname)
            replace_str(p, "com.youcompany.yourgame", package_name)
    except Exception as e:
        print(e)

def edit_build_settings(root):
    try:
        p = root + "ProjectSettings/EditorBuildSettings.asset"
        replace_str(p, build_before, build_after)
    except Exception as e:
        print(e)

def edit_cloaca_name(root, message):
    try:
        before = 'CLOACA_NAME'
        cloaca_name = xml_helper.get_tag_first_val(message, xml_helper.cloacaClassName)
        p1 = root + "Assets/Scripts/DataFetcher.cs"
        p2 = root + "Assets/Scripts/BackendHelper.cs"
        replace_str(p1, before, cloaca_name)
        replace_str(p2, before, cloaca_name)
    except Exception as e:
        print(e)

def edit_google_services_json(root, message):
    try:
        before = 'GOOGLE_JSON'
        g_service = xml_helper.get_tag_first_val(message, xml_helper.googleServices)
        p = root + "Assets/google-services.json"
        replace_str(p, before, g_service)

        json_serv = ast.literal_eval(g_service)
        p2 = root + "Assets/Plugins/Android/FirebaseApp.androidlib/res/values/google-services.xml"
        replace_str(p2, 'FIRA_DATA_URA', json_serv['project_info']['firebase_url'])
        replace_str(p2, 'GCM_DEFA_SENDAID', json_serv['project_info']['project_number'])
        replace_str(p2, 'G_STORE_BUCK', json_serv['project_info']['storage_bucket'])
        replace_str(p2, 'PRO_ID', json_serv['project_info']['project_id'])
        replace_str(p2, 'GO_API_KEY', json_serv['client'][0]['api_key'][0]['current_key'])
        replace_str(p2, 'G_CRASH_REPO_APIK', json_serv['client'][0]['api_key'][0]['current_key'])
        replace_str(p2, 'G_APPID', json_serv['client'][0]['client_info']['mobilesdk_app_id'])
        replace_str(p2, 'DEF_WEB_CLI_ID', json_serv['client'][0]['oauth_client'][0]['client_id'])
    except Exception as e:
        print(e)

def edit_bundle_id(root, message):
    try:
        before1 = 'BUNDLE_PARAM_ID'
        after1 = xml_helper.get_tag_first_val(message, xml_helper.appBundle)
        p1 = root + "Assets/Scripts/BackendHelper.cs"
        p12 = root + "Assets/Scripts/AppsflyerHelper.cs"
        replace_str(p1, before1, after1)
        replace_str(p12, before1, after1)

        before2 = 'applicationIdentifier:\n' \
                 '    Android:' \
                 '+[^\\n]+' \
                 '\n'
        after2 = 'applicationIdentifier:\n    Android: ' + after1 + '\n'

        before3 = 'applicationIdentifier: {}\n'

        after4 = 'AndroidTargetSdkVersion: 30\n'
        before4 = 'AndroidTargetSdkVersion: 0\n'

        p2 = root + "ProjectSettings/ProjectSettings.asset"

        replace_str(p2, before3, after2)
        replace_str(p2, before4, after4)

        with open(p2, 'r') as file:
            data = file.read()
        before = re.findall(before2, data)[0]
        data = data.replace(before, after2)
        with open(p2, 'w') as file:
            file.write(data)
    except Exception as e:
        print(e)

def edit_firebase_config(root, message):
    try:
        before1 = 'F_CONFIG_ROOT'
        before2 = 'F_CONFIG_U1'
        before3 = 'F_CONFIG_U2'
        after1 = xml_helper.get_tag_first_val(message, xml_helper.firebaseDbBaseNodeCAPS)
        after2 = xml_helper.get_tag_first_val(message, xml_helper.firebaseDomain1CAPS)
        after3 = xml_helper.get_tag_first_val(message, xml_helper.firebaseDomain2CAPS)
        p = root + "Assets/Scripts/FirebaseHelper.cs"
        replace_str(p, before1, after1)
        replace_str(p, before2, after2)
        replace_str(p, before3, after3)
    except Exception as e:
        print(e)

def edit_backend_config(root, message):
    try:
        before1 = 'BACK_RESP_U11'
        before2 = 'BACK_RESP_U12'
        before3 = 'BACK_RESP_U21'
        before4 = 'BACK_RESP_U22'
        after1 = xml_helper.get_tag_first_val(message, xml_helper.backResponseURL1_1)
        after2 = xml_helper.get_tag_first_val(message, xml_helper.backResponseURL1_2)
        after3 = xml_helper.get_tag_first_val(message, xml_helper.backResponseURL2_1)
        after4 = xml_helper.get_tag_first_val(message, xml_helper.backResponseURL2_2)
        p = root + "Assets/Scripts/BackendHelper.cs"
        replace_str(p, before1, after1)
        replace_str(p, before2, after2)
        replace_str(p, before3, after3)
        replace_str(p, before4, after4)
    except Exception as e:
        print(e)

def replace_str(dst, before, after):
    try:
        with open(dst, 'r') as file:
            data = file.read()
        data = data.replace(before, after)
        with open(dst, 'w') as file:
            file.write(data)
    except Exception as e:
        print(e)
