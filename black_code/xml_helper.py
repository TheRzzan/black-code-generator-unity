import re

archiveUrl = "arU"
appNumber = "apN"
appBundle = "apB"

cloacaClassName = "clCN"
facebookAppId = "fbAI"
googleServices = "goS"

firebaseDbBaseNodeCAPS = "fiDC"
firebaseDomain1CAPS = "fiD1"
firebaseDomain2CAPS = "fiD2"

backResponseURL1_1 = "baR11"
backResponseURL1_2 = "baR12"
backResponseURL2_1 = "baR21"
backResponseURL2_2 = "baR22"

maintainerName = "maN"
maintainerSurname = "maS"
keystorePass = "keyP"


def get_open_tag(name):
    return "<" + name + ">"


def get_close_tag(name):
    return "</" + name + ">"


def get_tag_first_val(string_val, tag_name):
    open_tag = get_open_tag(tag_name)
    close_tag = get_close_tag(tag_name)
    result = re.findall(open_tag + "+[^а-яА-Я]+" + close_tag, string_val)
    if len(result) <= 0:
        return None
    resp = result[0].replace(open_tag, "").replace(close_tag, "")
    if len(resp) <= 0:
        return None
    return resp
