import zipfile
import telebot
import requests
from black_code import black_code_helper, xml_helper

bot = telebot.TeleBot('1689931385:AAFoHYKvtm4I6nAeEQNhueity6Z49taNBms')


# Начало диалога
@bot.message_handler(commands=["start"])
def cmd_start(message):
    bot.send_message(message.chat.id, 'Привет, пришли конфиг файл, и я все сделаю')


@bot.message_handler()
def root_message(message):
    if xml_helper.get_tag_first_val(message.text, xml_helper.archiveUrl) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.appNumber) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.facebookAppId) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.appBundle) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.cloacaClassName) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.googleServices) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDbBaseNodeCAPS) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDomain1CAPS) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDomain2CAPS) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL1_1) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL1_2) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL2_1) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL2_2) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.maintainerName) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.maintainerSurname) is not None and \
            xml_helper.get_tag_first_val(message.text, xml_helper.keystorePass) is not None:
        bot.send_message(message.chat.id, "Все параметры в наличии")
    else:
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.archiveUrl)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.appNumber)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.facebookAppId)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.appBundle)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.cloacaClassName)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.googleServices)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDbBaseNodeCAPS)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDomain1CAPS)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.firebaseDomain2CAPS)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL1_1)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL1_2)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL2_1)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.backResponseURL2_2)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.maintainerName)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.maintainerSurname)))
        bot.send_message(message.chat.id, str(xml_helper.get_tag_first_val(message.text, xml_helper.keystorePass)))
        bot.send_message(message.chat.id, "Недостаточно параметров")
        return
    try:
        app_number = xml_helper.get_tag_first_val(message.text, xml_helper.appNumber)
        archive_url = xml_helper.get_tag_first_val(message.text, xml_helper.archiveUrl)
        save_dir = "/black/"
        src = app_number + ".zip"
        downloaded_file = requests.get(archive_url, allow_redirects=True)
        with open(save_dir + src, 'wb') as new_file:
            for chunk in downloaded_file.iter_content(chunk_size=128):
                new_file.write(chunk)
        fantasy_zip = zipfile.ZipFile(save_dir + src)
        fantasy_zip.extractall(save_dir + (src.replace(".zip", "")))
        fantasy_zip.close()
    except Exception as e:
        bot.send_message(message.chat.id, "Что-то не так с архивом, проверьте, что ссылка валидна и это zip")
        bot.send_message(message.chat.id, str(e.__traceback__))
        return
    try:
        bot.send_message(message.chat.id, "Проект загружен, добавляется серый код")
        black_code_helper.add_black_code(save_dir + (src.replace(".zip", "") + "/"), message.chat.id, message.text)
        bot.send_message(message.chat.id, "Проект отпарвлен на сборку в gitlab")
    except Exception as e:
        bot.send_message(message.chat.id, "Что-то пошло не так при добавлении кода")
        bot.send_message(message.chat.id, str(e.__traceback__))
        return


bot.polling(none_stop=True)
